%bcond_with cdb

%global postfix_user	postfix
%global postfix_group	postfix
%global maildrop_group	postdrop

%global postfix_config_dir	%{_sysconfdir}/postfix
%global postfix_daemon_dir	%{_libexecdir}/postfix
%global postfix_shlib_dir	%{_libdir}/postfix
%global postfix_command_dir	%{_sbindir}
%global postfix_queue_dir	%{_var}/spool/postfix
%global postfix_data_dir	%{_var}/lib/postfix

%global sasl_config_dir %{_sysconfdir}/sasl2

%global sslcert %{_sysconfdir}/pki/tls/certs/postfix.pem
%global sslkey  %{_sysconfdir}/pki/tls/private/postfix.key

# Filter private libraries
%global __provides_exclude ^(libpostfix-.+\.so.*)$
%global __requires_exclude ^(libpostfix-.+\.so.*)$

%global pflogsumm_version 1.1.5

# Postfix sasl configure
%global postfix_sasl_conf pwcheck_method: saslauthd\
mech_list: plain login

# Postfix PAM configure
%global postfix_pam_conf #%PAM-1.0\
auth       include	password-auth\
account    include	password-auth

Summary: A fast, easy to administer, and secure MTA
Name:    postfix
Version: 3.9.0
Release: 1%{?dist}
License: IPL-1.0 or EPL-2.0
URL:     http://www.postfix.org
Source0: http://ftp.porcupine.org/mirrors/postfix-release/official/%{name}-%{version}.tar.gz
Source1: https://jimsun.linxnet.com/downloads/pflogsumm-%{pflogsumm_version}.tar.gz
Source2: postfix.service
Source3: postfix.aliasesdb
Source4: postfix-chroot-update

Patch3000: postfix-3.7.0-config.patch
Patch3001: postfix-3.4.0-files.patch
Patch3002: postfix-3.3.3-alternatives.patch
Patch3003: postfix-3.7.0-large-fs.patch
Patch3004: postfix-3.4.4-chroot-example-fix.patch
Patch3005: postfix-3.7.0-whitespace-name-fix.patch
Patch3006: postfix-3.6.2-glibc-234-build-fix.patch
Patch3007: pflogsumm-1.1.5-datecalc.patch
Patch3008: pflogsumm-1.1.5-ipv6-warnings-fix.patch
Patch3009: pflogsumm-1.1.5-syslog-name-underscore-fix.patch

BuildRequires: make gcc m4 findutils systemd-units pkgconfig perl-generators
BuildRequires: libdb-devel zlib-devel libicu-devel libnsl2-devel openldap-devel
BuildRequires: lmdb-devel cyrus-sasl-devel pcre-devel mariadb-connector-c-devel
BuildRequires: libpq-devel sqlite-devel openssl-devel
%if %{with cdb}
BuildRequires: tinycdb-devel
%endif
Requires(pre): %{_sbindir}/groupadd %{_sbindir}/useradd
Requires(post): systemd systemd-sysv hostname %{_bindir}/openssl
Requires(post): %{_sbindir}/alternatives
Requires(preun): %{_sbindir}/alternatives systemd
Requires(postun): systemd
Requires: diffutils findutils policycoreutils
Provides: MTA smtpd smtpdaemon server(smtp)

%description
Postfix is Wietse Venema's attempt to provide an alternative to the widely-used 
sendmail program.

%package perl-scripts
Summary: Postfix utilities written in perl
Requires: %{name} = %{version}-%{release}
Provides: %{name}-pflogsumm = %{version}-%{release}

%description perl-scripts
This package contains perl scripts pflogsumm and qshape.

Pflogsumm is a log analyzer/summarizer for the Postfix MTA.  It is
designed to provide an over-view of Postfix activity, with just enough
detail to give the administrator a "heads up" for potential trouble
spots. Pflogsumm generates summaries and, in some cases, detailed reports
of mail server traffic volumes, rejected and bounced email, and server
warnings, errors and panics.

The qshape program helps the administrator understand the Postfix queue 
message distribution in time and by sender domain or recipient domain.

%package mysql
Summary: Postfix MySQL map support
Requires: %{name} = %{version}-%{release}

%description mysql
The Postfix mysql map type allows you to hook up Postfix to a MySQL 
database. This implementation allows for multiple mysql databases: 
you can use one for a virtual(5) table, one for an access(5) table, 
and one for an aliases(5) table if you want. You can specify multiple 
servers for the same database, so that Postfix can switch to a good 
database server if one goes bad.

%package pgsql
Summary: Postfix PostgreSQL map support
Requires: %{name} = %{version}-%{release}

%description pgsql
The Postfix pgsql map type allows you to hook up Postfix to a PostgreSQL
database. This implementation allows for multiple pgsql databases: you 
can use one for a virtual(5) table, one for an access(5) table, and one 
for an aliases(5) table if you want. You can specify multiple servers for 
the same database, so that Postfix can switch to a good database server 
if one goes bad.

%package sqlite
Summary: Postfix SQLite map support
Requires: %{name} = %{version}-%{release}

%description sqlite
The Postfix sqlite map type allows you to hook up Postfix to a SQLite 
database. This implementation allows for multiple sqlite databases: 
you can use one for a virtual(5) table, one for an access(5) table, 
and one for an aliases(5) table if you want.

%if %{with cdb}
%package cdb
Summary: Postfix CDB map support
Requires: %{name} = %{version}-%{release}

%description cdb
CDB (Constant DataBase) is an indexed file format designed by Daniel 
Bernstein. CDB is optimized exclusively for read access and guarantees 
that each record will be read in at most two disk accesses. This is 
achieved by forgoing support for incremental updates: no single-record 
inserts or deletes are supported. CDB databases can be modified only by 
rebuilding them completely from scratch, hence the "constant" qualifier 
in the name.
%endif

%package ldap
Summary: Postfix LDAP map support
Requires: %{name} = %{version}-%{release}

%description ldap
Postfix can use an LDAP directory as a source for any of its lookups: 
aliases(5), virtual(5), canonical(5), etc. This allows you to keep 
information for your mail service in a replicated network database 
with fine-grained access controls. By not storing it locally on the 
mail server, the administrators can maintain it from anywhere, and 
the users can control whatever bits of it you think appropriate. You 
can have multiple mail servers using the same information, without 
the hassle and delay of having to copy it to each.

%package lmdb
Summary: Postfix LDMB map support
Requires: %{name} = %{version}-%{release}

%description lmdb
Postfix uses databases of various kinds to store and look up information. 
Postfix databases are specified as "type:name". OpenLDAP LMDB (called 
"LMDB" from here on) implements the Postfix database type "lmdb". The 
name of a Postfix LMDB database is the name of the database file without 
the ".lmdb" suffix.

%package pcre
Summary: Postfix PCRE map support
Requires: %{name} = %{version}-%{release}

%description pcre
The optional "pcre" map type allows you to specify regular expressions 
with the PERL style notation such as \s for space and \S for non-space. 
The main benefit, however, is that pcre lookups are often faster than 
regexp lookups. This is because the pcre implementation is often more 
efficient than the POSIX regular expression implementation that you 
find on many systems.

%prep
%setup -q
%autopatch -m 3000 -M 3006 -p1

gzip -dc %{SOURCE1} | tar xf -
pushd pflogsumm-%{pflogsumm_version}
%patch3007 -p1
%patch3008 -p1
popd
%patch3009 -p1

# support for kernel 6.X
sed -i makedefs -e '\@Linux\.@s|345|3456|'
sed -i src/util/sys_defs.h -e 's@defined(LINUX5)@defined(LINUX5) || defined(LINUX6)@'

sed -i \
's|^\(\s*#define\s\+DEF_SHLIB_DIR\s\+\)"/usr/lib/postfix"|\1"%{_libdir}/postfix"|' \
src/global/mail_params.h

%build
unset AUXLIBS AUXLIBS_LDAP AUXLIBS_LMDB AUXLIBS_PCRE AUXLIBS_MYSQL AUXLIBS_PGSQL AUXLIBS_SQLITE AUXLIBS_CDB

CCARGS="-fPIC -fcommon"
CCARGS="${CCARGS} -DHAS_LDAP -DLDAP_DEPRECATED=1 -DUSE_LDAP_SASL"
CCARGS="${CCARGS} -DHAS_LMDB"
CCARGS="${CCARGS} -DHAS_PCRE -I%{_includedir}/pcre"
CCARGS="${CCARGS} -DHAS_MYSQL -I%{_includedir}/mysql"
CCARGS="${CCARGS} -DHAS_PGSQL -I%{_includedir}/pgsql"
CCARGS="${CCARGS} -DHAS_SQLITE `pkg-config --cflags sqlite3`"
%if %{with cdb}
CCARGS="${CCARGS} -DHAS_CDB `pkg-config --cflags libcdb`"
%endif
CCARGS="${CCARGS} -DUSE_SASL_AUTH -DUSE_CYRUS_SASL -I%{_includedir}/sasl"
CCARGS="${CCARGS} -DDEF_CONFIG_DIR=\\\"%{postfix_config_dir}\\\""
CCARGS="${CCARGS} `getconf LFS_CFLAGS`"

if pkg-config openssl ; then
  CCARGS="${CCARGS} -DUSE_TLS `pkg-config --cflags openssl`"
  AUXLIBS="`pkg-config --libs openssl`"
else
  CCARGS="${CCARGS} -DUSE_TLS -I%{_includedir}/openssl"
  AUXLIBS="-lssl -lcrypto"
fi

make -f Makefile.init makefiles \
  shared=yes dynamicmaps=yes pie=yes \
  CCARGS="${CCARGS}" \
  AUXLIBS="-lnsl -L%{_libdir}/sasl2 -lsasl2 ${AUXLIBS}" \
  AUXLIBS_LDAP="-lldap -llber" \
  AUXLIBS_LMDB="-llmdb" \
  AUXLIBS_PCRE="-lpcre" \
  AUXLIBS_MYSQL="-L%{_libdir}/mariadb -lmysqlclient -lm" \
  AUXLIBS_PGSQL="-lpq" \
  AUXLIBS_SQLITE="`pkg-config --libs sqlite3`" \
%if %{with cdb}
  AUXLIBS_CDB="`pkg-config --libs libcdb`" \
%endif
  DEBUG="" \
  SHLIB_RPATH="-Wl,-rpath,%{postfix_shlib_dir} $LDFLAGS -Wl,-z,relro,-z,now" \
  OPT="$CFLAGS -fno-strict-aliasing -Wno-comment" \
  POSTFIX_INSTALL_OPTS=-keep-build-mtime

%make_build

%install
for i in man1/mailq.1 man1/newaliases.1 man1/sendmail.1 man5/aliases.5 man8/smtpd.8; do
  dest=$(echo $i | sed 's|\.[1-9]$|.postfix\0|')
  mv man/$i man/$dest
  sed -i "s|^\.so $i|\.so $dest|" man/man?/*.[1-9]
done

make non-interactive-package \
       install_root=%{buildroot} \
       config_directory=%{postfix_config_dir} \
       meta_directory=%{postfix_config_dir} \
       shlib_directory=%{postfix_shlib_dir} \
       daemon_directory=%{postfix_daemon_dir} \
       command_directory=%{postfix_command_dir} \
       queue_directory=%{postfix_queue_dir} \
       data_directory=%{postfix_data_dir} \
       sendmail_path=%{postfix_command_dir}/sendmail.postfix \
       newaliases_path=%{_bindir}/newaliases.postfix \
       mailq_path=%{_bindir}/mailq.postfix \
       mail_owner=%{postfix_user} \
       setgid_group=%{maildrop_group} \
       manpage_directory=%{_mandir} \
       sample_directory=%{_pkgdocdir}/samples \
       readme_directory=%{_pkgdocdir}/README_FILES || exit 1

mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE2} %{buildroot}%{_unitdir}
install -m 755 %{SOURCE3} %{buildroot}%{postfix_daemon_dir}/aliasesdb
install -m 755 %{SOURCE4} %{buildroot}%{postfix_daemon_dir}/chroot-update

install -c auxiliary/rmail/rmail %{buildroot}/%{_bindir}/rmail.postfix

for i in active bounce corrupt defer deferred flush incoming private saved maildrop public pid saved trace; do
    mkdir -p %{buildroot}/%{postfix_queue_dir}/$i
done

for i in smtp-sink smtp-source posttls-finger ; do
  install -c -m 755 bin/$i %{buildroot}/%{postfix_command_dir}/
  install -c -m 755 man/man1/$i.1 %{buildroot}/%{_mandir}/man1/
done

sed -i -r "s#(/man[158]/.*.[158]):f#\1.gz:f#" %{buildroot}/%{postfix_config_dir}/postfix-files

mkdir -p %{buildroot}/%{sasl_config_dir}
echo '%{postfix_sasl_conf}' > %{buildroot}/%{sasl_config_dir}/smtpd.conf
chmod 644 %{buildroot}/%{sasl_config_dir}/smtpd.conf

mkdir -p %{buildroot}/%{_sysconfdir}/pam.d
echo '%{postfix_pam_conf}' > %{buildroot}/%{_sysconfdir}/pam.d/smtp.postfix
chmod 644 %{buildroot}/%{_sysconfdir}/pam.d/smtp.postfix

mkdir -p %{buildroot}/%{_pkgdocdir}
cp -p COMPATIBILITY TLS_ACKNOWLEDGEMENTS %{buildroot}/%{_pkgdocdir}

mkdir -p %{buildroot}/%{_pkgdocdir}/examples{,/chroot-setup}
cp -pr examples/{qmail-local,smtpd-policy} %{buildroot}/%{_pkgdocdir}/examples
cp -p examples/chroot-setup/LINUX2 %{buildroot}/%{_pkgdocdir}/examples/chroot-setup

cp conf/{main,bounce}.cf.default %{buildroot}/%{_pkgdocdir}
sed -i 's#%{postfix_config_dir}\(/bounce\.cf\.default\)#%{_pkgdocdir}\1#' %{buildroot}/%{_mandir}/man5/bounce.5
rm -f %{buildroot}/%{postfix_config_dir}/{TLS_,}LICENSE

find %{buildroot}/%{_pkgdocdir} -type f | xargs chmod 644
find %{buildroot}/%{_pkgdocdir} -type d | xargs chmod 755

install -c -m 644 pflogsumm-%{pflogsumm_version}/pflogsumm-faq.txt %{buildroot}/%{_pkgdocdir}/pflogsumm-faq.txt
install -c -m 644 pflogsumm-%{pflogsumm_version}/pflogsumm.1 %{buildroot}/%{_mandir}/man1/pflogsumm.1
install -c pflogsumm-%{pflogsumm_version}/pflogsumm.pl %{buildroot}/%{postfix_command_dir}/pflogsumm

mantools/srctoman - auxiliary/qshape/qshape.pl > qshape.1
install -c qshape.1 %{buildroot}/%{_mandir}/man1/qshape.1
install -c auxiliary/qshape/qshape.pl %{buildroot}/%{postfix_command_dir}/qshape

rm -f %{buildroot}/%{postfix_config_dir}/aliases

mkdir -p %{buildroot}/%{_prefix}/lib
pushd %{buildroot}/%{_prefix}/lib
ln -sf ../sbin/sendmail.postfix .
popd

mkdir -p %{buildroot}/%{_var}/lib/misc
touch %{buildroot}/%{_var}/lib/misc/postfix.aliasesdb-stamp

for i in %{postfix_command_dir}/sendmail %{_bindir}/{mailq,newaliases,rmail} \
	%{_sysconfdir}/pam.d/smtp %{_prefix}/lib/sendmail \
	%{_mandir}/{man1/{mailq.1,newaliases.1},man5/aliases.5,man8/{sendmail.8,smtpd.8}}
do
	touch %{buildroot}/$i
done

function split_file
{
  grep "$1" "$3" >> "$3.d/$2" || :
  sed -i "\|$1| d" "$3" || :
}

pushd %{buildroot}/%{postfix_config_dir}
for map in mysql pgsql sqlite %{?with_cdb:cdb} ldap lmdb pcre; do
  rm -f dynamicmaps.cf.d/"$map" "postfix-files.d/$map"
  split_file "^\s*$map\b" "$map" dynamicmaps.cf
  sed -i "s|postfix-$map\\.so|%{postfix_shlib_dir}/\\0|" "dynamicmaps.cf.d/$map"
  split_file "^\$shlib_directory/postfix-$map\\.so:" "$map" postfix-files
  split_file "^\$manpage_directory/man5/${map}_table\\.5" "$map" postfix-files
  map_upper=`echo $map | tr '[:lower:]' '[:upper:]'`
  split_file "^\$readme_directory/${map_upper}_README:" "$map" postfix-files
done
popd

%pre
%{_sbindir}/groupadd -g 90 -r %{maildrop_group} 2>/dev/null
%{_sbindir}/groupadd -g 89 -r %{postfix_group} 2>/dev/null
%{_sbindir}/groupadd -g 12 -r mail 2>/dev/null
%{_sbindir}/useradd -d %{postfix_queue_dir} -s /sbin/nologin -g %{postfix_group} -G mail -M -r -u 89 %{postfix_user} 2>/dev/null

if [ -e %{_mandir}/man8/smtpd.8.gz ]; then
	[ -h %{_mandir}/man8/smtpd.8.gz ] || rm -f %{_mandir}/man8/smtpd.8.gz
fi

exit 0

%post -e
%systemd_post %{name}.service

%{_sbindir}/postfix set-permissions upgrade-configuration \
	daemon_directory=%{postfix_daemon_dir} \
	command_directory=%{postfix_command_dir} \
	mail_owner=%{postfix_user} \
	setgid_group=%{maildrop_group} \
	manpage_directory=%{_mandir} \
	sample_directory=%{_pkgdocdir}/samples \
	readme_directory=%{_pkgdocdir}/README_FILES &> /dev/null

ALTERNATIVES_DOCS=""
[ "%%{_excludedocs}" = 1 ] || ALTERNATIVES_DOCS='--slave %{_mandir}/man1/mailq.1.gz mta-mailqman %{_mandir}/man1/mailq.postfix.1.gz
	--slave %{_mandir}/man1/newaliases.1.gz mta-newaliasesman %{_mandir}/man1/newaliases.postfix.1.gz
	--slave %{_mandir}/man8/sendmail.8.gz mta-sendmailman %{_mandir}/man1/sendmail.postfix.1.gz
	--slave %{_mandir}/man5/aliases.5.gz mta-aliasesman %{_mandir}/man5/aliases.postfix.5.gz
	--slave %{_mandir}/man8/smtpd.8.gz mta-smtpdman %{_mandir}/man8/smtpd.postfix.8.gz'

%{_sbindir}/alternatives --install %{postfix_command_dir}/sendmail mta %{postfix_command_dir}/sendmail.postfix 60 \
	--slave %{_bindir}/mailq mta-mailq %{_bindir}/mailq.postfix \
	--slave %{_bindir}/newaliases mta-newaliases %{_bindir}/newaliases.postfix \
	--slave %{_sysconfdir}/pam.d/smtp mta-pam %{_sysconfdir}/pam.d/smtp.postfix \
	--slave %{_bindir}/rmail mta-rmail %{_bindir}/rmail.postfix \
	--slave %{_prefix}/lib/sendmail mta-sendmail %{_prefix}/lib/sendmail.postfix \
	$ALTERNATIVES_DOCS \
	--initscript postfix

if [ -f %{_libdir}/sasl2/smtpd.conf ]; then
	mv -f %{_libdir}/sasl2/smtpd.conf %{sasl_config_dir}/smtpd.conf
	/sbin/restorecon %{sasl_config_dir}/smtpd.conf 2> /dev/null
fi

if [ ! -f %{sslkey} ]; then
  umask 077
  %{_bindir}/openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:4096 -out %{sslkey} 2>/dev/null || echo "openssl genpkey failed"
fi

if [ ! -f %{sslcert} ]; then
  FQDN=`hostname`
  if [ "x${FQDN}" = "x" ]; then
    FQDN=localhost.localdomain
  fi

  req_cmd="%{_bindir}/openssl req -new -key %{sslkey} -x509 -sha256 -days 365 -set_serial $RANDOM -out %{sslcert} \
    -subj /C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=${FQDN}/emailAddress=root@${FQDN}"
  $req_cmd -noenc -copy_extensions none 2>/dev/null || $req_cmd 2>/dev/null || echo "openssl req failed"
  chmod 644 %{sslcert}
fi

exit 0

%preun
%systemd_preun %{name}.service

if [ "$1" = 0 ]; then
    %{_sbindir}/alternatives --remove mta %{postfix_command_dir}/sendmail.postfix
fi

exit 0

%postun
%systemd_postun_with_restart %{name}.service

%files
%license LICENSE TLS_LICENSE
%doc %{_pkgdocdir}
%exclude %{_pkgdocdir}/pflogsumm-faq.txt
%exclude %{_pkgdocdir}/README_FILES/MYSQL_README
%exclude %{_pkgdocdir}/README_FILES/PGSQL_README
%exclude %{_pkgdocdir}/README_FILES/SQLITE_README
%exclude %{_pkgdocdir}/README_FILES/CDB_README
%exclude %{_pkgdocdir}/README_FILES/LDAP_README
%exclude %{_pkgdocdir}/README_FILES/LMDB_README
%exclude %{_pkgdocdir}/README_FILES/PCRE_README
%exclude %{_pkgdocdir}/LICENSE
%exclude %{_pkgdocdir}/TLS_LICENSE

%defattr(0755, root, root, -)
%dir %{postfix_config_dir}
%dir %{postfix_config_dir}/dynamicmaps.cf.d
%dir %{postfix_config_dir}/postfix-files.d
%dir %{postfix_daemon_dir}
%dir %{postfix_shlib_dir}
%dir %{postfix_queue_dir}
%dir %{postfix_queue_dir}/pid

%defattr(0700, %{postfix_user}, root, -)
%dir %{postfix_queue_dir}/active
%dir %{postfix_queue_dir}/bounce
%dir %{postfix_queue_dir}/corrupt
%dir %{postfix_queue_dir}/defer
%dir %{postfix_queue_dir}/deferred
%dir %{postfix_queue_dir}/flush
%dir %{postfix_queue_dir}/hold
%dir %{postfix_queue_dir}/incoming
%dir %{postfix_queue_dir}/saved
%dir %{postfix_queue_dir}/trace
%dir %{postfix_queue_dir}/private
%dir %attr(0710, %{postfix_user}, %{maildrop_group}) %{postfix_queue_dir}/public
%dir %attr(0730, %{postfix_user}, %{maildrop_group}) %{postfix_queue_dir}/maildrop
%dir %{postfix_data_dir}

%defattr(0644, root, root, -)
%config(noreplace) %{sasl_config_dir}/smtpd.conf
%config(noreplace) %{_sysconfdir}/pam.d/smtp.postfix
%ghost %{_sysconfdir}/pam.d/smtp
%config(noreplace) %{postfix_config_dir}/access
%config(noreplace) %{postfix_config_dir}/canonical
%config(noreplace) %{postfix_config_dir}/generic
%config(noreplace) %{postfix_config_dir}/header_checks
%config(noreplace) %{postfix_config_dir}/main.cf
%config(noreplace) %{postfix_config_dir}/main.cf.proto
%config(noreplace) %{postfix_config_dir}/master.cf
%config(noreplace) %{postfix_config_dir}/master.cf.proto
%config(noreplace) %{postfix_config_dir}/relocated
%config(noreplace) %{postfix_config_dir}/transport
%config(noreplace) %{postfix_config_dir}/virtual
%{postfix_config_dir}/dynamicmaps.cf
%{postfix_config_dir}/postfix-files
%{_unitdir}/postfix.service
%ghost %{_var}/lib/misc/postfix.aliasesdb-stamp

%defattr(0755, root, root, -)
%{postfix_command_dir}/smtp-sink
%{postfix_command_dir}/smtp-source
%{postfix_command_dir}/posttls-finger
%{postfix_command_dir}/postalias
%{postfix_command_dir}/postcat
%{postfix_command_dir}/postconf
%{postfix_command_dir}/postfix
%{postfix_command_dir}/postkick
%{postfix_command_dir}/postlock
%{postfix_command_dir}/postlog
%{postfix_command_dir}/postmap
%{postfix_command_dir}/postmulti
%{postfix_command_dir}/postsuper
%attr(2755, root, %{maildrop_group}) %{postfix_command_dir}/postdrop
%attr(2755, root, %{maildrop_group}) %{postfix_command_dir}/postqueue

%{_bindir}/mailq.postfix
%ghost %{_bindir}/mailq
%{_bindir}/newaliases.postfix
%ghost %{_bindir}/newaliases
%{_bindir}/rmail.postfix
%ghost %{_bindir}/rmail
%{_sbindir}/sendmail.postfix
%ghost %{_sbindir}/sendmail
%{_prefix}/lib/sendmail.postfix
%ghost %{_prefix}/lib/sendmail

%{postfix_daemon_dir}/[^mp]*
%{postfix_daemon_dir}/master
%{postfix_daemon_dir}/pickup
%{postfix_daemon_dir}/pipe
%{postfix_daemon_dir}/post-install
%{postfix_daemon_dir}/postfix-script
%{postfix_daemon_dir}/postfix-tls-script
%{postfix_daemon_dir}/postfix-wrapper
%{postfix_daemon_dir}/postmulti-script
%{postfix_daemon_dir}/postscreen
%{postfix_daemon_dir}/postlogd
%{postfix_daemon_dir}/proxymap
%{postfix_shlib_dir}/libpostfix-*.so

%defattr(0644, root, root, -)
%exclude %{_mandir}/man5/mysql_table.5*
%exclude %{_mandir}/man5/pgsql_table.5*
%exclude %{_mandir}/man5/sqlite_table.5*
%exclude %{_mandir}/man5/ldap_table.5*
%exclude %{_mandir}/man5/lmdb_table.5*
%exclude %{_mandir}/man5/pcre_table.5*

%{_mandir}/man1/post*.1*
%{_mandir}/man1/smtp*.1*
%{_mandir}/man1/*.postfix.1*
%ghost %{_mandir}/man1/mailq.1.gz
%ghost %{_mandir}/man1/newaliases.1.gz
%{_mandir}/man5/access.5*
%{_mandir}/man5/[b-v]*.5*
%{_mandir}/man5/*.postfix.5*
%ghost %{_mandir}/man5/aliases.5.gz
%{_mandir}/man8/[a-qt-v]*.8*
%{_mandir}/man8/s[ch-lnp]*.8*
%{_mandir}/man8/smtp.8*
%{_mandir}/man8/smtpd.postfix.8*
%ghost %{_mandir}/man8/smtpd.8.gz
%ghost %{_mandir}/man8/sendmail.8.gz

%files perl-scripts
%doc %{_pkgdocdir}/pflogsumm-faq.txt
%attr(0755, root, root) %{postfix_command_dir}/pflogsumm
%attr(0755, root, root) %{postfix_command_dir}/qshape
%attr(0644, root, root) %{_mandir}/man1/pflogsumm.1.gz
%attr(0644, root, root) %{_mandir}/man1/qshape*

%files mysql
%doc %{_pkgdocdir}/README_FILES/MYSQL_README
%attr(0644, root, root) %{postfix_config_dir}/dynamicmaps.cf.d/mysql
%attr(0644, root, root) %{postfix_config_dir}/postfix-files.d/mysql
%attr(0755, root, root) %{postfix_shlib_dir}/postfix-mysql.so
%attr(0644, root, root) %{_mandir}/man5/mysql_table.5*

%files pgsql
%doc %{_pkgdocdir}/README_FILES/PGSQL_README
%attr(0644, root, root) %{postfix_config_dir}/dynamicmaps.cf.d/pgsql
%attr(0644, root, root) %{postfix_config_dir}/postfix-files.d/pgsql
%attr(0755, root, root) %{postfix_shlib_dir}/postfix-pgsql.so
%attr(0644, root, root) %{_mandir}/man5/pgsql_table.5*

%files sqlite
%doc %{_pkgdocdir}/README_FILES/SQLITE_README
%attr(0644, root, root) %{postfix_config_dir}/dynamicmaps.cf.d/sqlite
%attr(0644, root, root) %{postfix_config_dir}/postfix-files.d/sqlite
%attr(0755, root, root) %{postfix_shlib_dir}/postfix-sqlite.so
%attr(0644, root, root) %{_mandir}/man5/sqlite_table.5*

%if %{with cdb}
%files cdb
%doc %{_pkgdocdir}/README_FILES/CDB_README
%attr(0644, root, root) %{postfix_config_dir}/dynamicmaps.cf.d/cdb
%attr(0644, root, root) %{postfix_config_dir}/postfix-files.d/cdb
%attr(0755, root, root) %{postfix_shlib_dir}/postfix-cdb.so
%endif

%files ldap
%doc %{_pkgdocdir}/README_FILES/LDAP_README
%attr(0644, root, root) %{postfix_config_dir}/dynamicmaps.cf.d/ldap
%attr(0644, root, root) %{postfix_config_dir}/postfix-files.d/ldap
%attr(0755, root, root) %{postfix_shlib_dir}/postfix-ldap.so
%attr(0644, root, root) %{_mandir}/man5/ldap_table.5*

%files lmdb
%doc %{_pkgdocdir}/README_FILES/LMDB_README
%attr(0644, root, root) %{postfix_config_dir}/dynamicmaps.cf.d/lmdb
%attr(0644, root, root) %{postfix_config_dir}/postfix-files.d/lmdb
%attr(0755, root, root) %{postfix_shlib_dir}/postfix-lmdb.so
%attr(0644, root, root) %{_mandir}/man5/lmdb_table.5*

%files pcre
%doc %{_pkgdocdir}/README_FILES/PCRE_README
%attr(0644, root, root) %{postfix_config_dir}/dynamicmaps.cf.d/pcre
%attr(0644, root, root) %{postfix_config_dir}/postfix-files.d/pcre
%attr(0755, root, root) %{postfix_shlib_dir}/postfix-pcre.so
%attr(0644, root, root) %{_mandir}/man5/pcre_table.5*

%changelog
* Mon Jul 01 2024 Upgrade Robot <upbot@opencloudos.tech> - 3.9.0-1
- Upgrade to version 3.9.0

* Tue Jan 30 2024 Miaojun Dong <zoedong@opencloudos.org> - 3.8.5-1
- Upgrade to version 3.8.5 (Fix CVE-2023-51764)

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.7.2-8
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Sep 04 2023 cunshunxia <cunshunxia@tencent.com> - 3.7.2-7
- support kernel 6.1.

* Wed Aug 23 2023 rockerzhu <rockerzhu@tencent.com> - 3.7.2-6
- Rebuilt for icu 73.2

* Fri Aug 18 2023 Wang Guodong <gordonwwang@tencent.com> - 3.7.2-5
- Rebuilt for mariadb-connector-c 3.3.5

* Thu Aug 3 2023 Shuo Wang <abushwang@tencent.com> - 3.7.2-4
- Rebuilt for libpq 15.3

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.7.2-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.7.2-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Jun 6 2022 Xiaojie Chen <jackxjchen@tencent.com> - 3.7.2-1
- Initial build
